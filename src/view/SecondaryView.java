package view;

/**
 * Contains internally a view, that is updated retrieving informations from the
 * controller, which will be based on the actual state of the model.
 */
public interface SecondaryView extends UpdatableView, ViewElement {

}
