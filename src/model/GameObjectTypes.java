package model;

/**
 * Defines the possible types of objects of that can be found in the game.
 */
public enum GameObjectTypes {
    /**
     * the unit.
     */
    UNIT, 
    /**
     * the terrain.
     */
    TERRAIN, 
    /**
     * the structure.
     */
    STRUCTURE;
}
